# define parameters
$userName = "testUser" 

# unlock user
NET USER $userName /EXPIRES:NEVER /ACTIVE:YES

# allow user changing clock time
REG ADD "HKCU\SOFTWARE\Policies\Microsoft\Control Panel\International" /f /v "PreventUserOverrides" /t REG_DWORD /d 0
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Control Panel\International" /f /v "PreventUserOverrides" /t REG_DWORD /d 0

# clear logon message
Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" -Name "legalnoticecaption" -Value ""
Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" -Name "legalnoticetext" -Value ""

# delete the logoff task
Unregister-ScheduledTask -TaskName 'Logoff Expired User' -Confirm:$false