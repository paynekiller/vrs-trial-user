# define parameters
$userName = "testUser"
$userPassword = "p@ssword1234"
$expiryDate = "29/07/2022"
$messageTitle = "Notice"
$messageBody = ("System will lock on " + $expiryDate + ", please call <number> for more information.")

# create user
NET USER $userName $userPassword /ADD /EXPIRES:$expiryDate

# stop user changing clock time
REG ADD "HKCU\SOFTWARE\Policies\Microsoft\Control Panel\International" /f /v "PreventUserOverrides" /t REG_DWORD /d 1
REG ADD "HKLM\SOFTWARE\Policies\Microsoft\Control Panel\International" /f /v "PreventUserOverrides" /t REG_DWORD /d 1

# set logon message
Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" -Name "legalnoticecaption" -Value $messageTitle
Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows\CurrentVersion\Policies\System" -Name "legalnoticetext" -Value $messageBody

# setup task to logoff user at expiry
$isoDate = [DateTime]::ParseExact($expiryDate, "dd/MM/yyyy", $null)
$arg = ("-Command  `"Logoff`"")
$Action = New-ScheduledTaskAction -Execute "powershell.exe" -Argument $arg
$Trigger = New-ScheduledTaskTrigger -Once -At ($isoDate.ToString("yyyy/MM/dd") + "T00:00:00Z")
$Settings = New-ScheduledTaskSettingsSet
$Task = New-ScheduledTask -Action $Action -Trigger $Trigger -Settings $Settings
Register-ScheduledTask -TaskName 'Logoff Expired User' -InputObject $Task -User $userName 
